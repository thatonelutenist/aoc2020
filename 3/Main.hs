{-# LANGUAGE NamedFieldPuns,RecordWildCards #-}
module Main where

data NodeType = Open | Tree deriving (Show,Eq)
data Node = Node NodeContents | End
data NodeContents = NodeContents { right :: Node
                                 , down :: Node
                                 , kind :: NodeType
                                 }
node :: (NodeContents -> Node) -> Node -> Node
node _ End = End
node a (Node b) = a b
                  
nodeType :: Node -> NodeType
nodeType (Node NodeContents { .. }) = kind

getInput :: IO [[NodeType]]
getInput = do
  content <- readFile "/Users/nathan/Projects/aoc2020/inputs/input-3"
  return . map parseLine . lines $ content
  where parseLine :: String -> [NodeType]
        parseLine line = map parseCharacter line
        parseCharacter :: Char -> NodeType
        parseCharacter '#' = Tree
        parseCharacter '.' = Open

buildRow :: Node -> Node -> [NodeType] -> Node
buildRow down right [kind] = Node NodeContents { .. }
buildRow down first (kind:rest) =
  let nextRight = buildRow (node right down) first rest
  in Node NodeContents { right = nextRight, .. }

buildGraph :: [[NodeType]] -> Node
buildGraph [line] =
  let (kind:rest) = line
      first = Node NodeContents { right = buildLastRow first rest
                                , down = End
                                , kind }
  in first
  where buildLastRow = buildRow End
buildGraph ((kind:rline):rest) =
  let next = buildGraph rest
      first = Node NodeContents { right = buildRow (node right next) first rline
                                , down = next
                                , kind }
  in first
  where buildRow :: Node -> Node -> [NodeType] -> Node
        buildRow down right [kind] = Node NodeContents { .. }
        buildRow down first (kind:rest) =
          let nextRight = buildRow (node right down) first rest
          in Node NodeContents { right = nextRight, .. }

traverseGraph :: (Int, Int) -> Node -> [NodeType]
traverseGraph _ End = []
traverseGraph (stepRight, stepDown) nextNode =
  let kind = nodeType nextNode
      next = go (node right) stepRight . go (node down) stepDown $ nextNode
  in kind:traverseGraph (stepRight,stepDown) next
  where go :: (Node -> Node) -> Int -> Node -> Node
        go _ _ End = End
        go _ 0 node = node
        go d x node = go d (x-1) (d node)
        

main :: IO ()
main = do
  input <- getInput
  let node = buildGraph input
  let encounters = traverseGraph (3,1) node
  putStrLn "Part 1:"
  print . length . filter (== Tree) $ encounters
  let slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]
  let encounters = map (\x -> length . filter (== Tree) . traverseGraph x $ node) slopes
  putStrLn "Part 2:"
  print $ product encounters
