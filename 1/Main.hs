{-# LANGUAGE TupleSections #-}
module Main where

getInput :: IO [Int]
getInput = do
  content <- readFile "/Users/nathan/Projects/aoc2020/inputs/input-1"
  return . map read . lines $ content

entriesSummingTo :: [Int] -> Int -> [(Int,Int)]
entriesSummingTo [] y = []
entriesSummingTo (x:xs) y =
  let head = map (x,) . filter (\z -> x + z == y) $ xs
  in head ++ entriesSummingTo xs y

triplets :: [Int] -> [(Int, Int, Int)]
triplets [] = []
triplets (x:xs) =
  let pairs = doublets xs
      head = map (\(y,z) -> (x,y,z)) pairs
  in head ++ triplets xs
  where doublets :: [Int] -> [(Int,Int)]
        doublets [] = []
        doublets (x:xs) =
          let head = map (x,) xs
          in head ++ doublets xs
  
main :: IO ()
main = do
  numbers <- getInput
  let pairs = entriesSummingTo numbers 2020
  let (x,y) = head pairs
  putStrLn "Part 1:"
  print $ x * y
  putStrLn "Part 2:"
  let (x,y,z) =  head . filter (\(x,y,z) -> x + y + z == 2020) . triplets $ numbers
  print $ x * y * z
