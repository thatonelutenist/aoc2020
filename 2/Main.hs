{-# LANGUAGE NamedFieldPuns,RecordWildCards #-}
module Main where

import Debug.Trace

getInput :: IO [String]
getInput = do
  content <- readFile "/Users/nathan/Projects/aoc2020/inputs/input-2"
  return . lines $ content

data Entry = Entry { minCount :: Int
                   , maxCount :: Int
                   , letter :: Char
                   , password :: String
                   } deriving (Show)

parseEntry :: String -> Entry
parseEntry line =
  let (smin,rmin) = splitAt '-' line
      minCount = read smin :: Int
      (smax,rmax) = splitAt ' ' rmin
      maxCount = read smax :: Int
      (letter:_,_:password) = splitAt ':' rmax
  in Entry { minCount, maxCount, letter, password }
  where splitAt :: Char -> String -> (String, String)
        splitAt c string =
          let head = takeWhile (/= c) string
              (_:tail) = dropWhile (/= c) string
          in (head, tail)

validateEntry :: Entry -> Bool
validateEntry Entry {..} =
  let count = length . filter (== letter) $ password
  in count <= maxCount && count >= minCount

validateEntry2 :: Entry -> Bool
validateEntry2 Entry {..} =
  let first = (password !! (minCount - 1)) == letter
      second = (password !! (maxCount - 1)) == letter
  in first /= second

main :: IO ()
main = do
  lines <- getInput
  let entries = map parseEntry lines
  putStrLn "Part 1:"
  print . length . filter validateEntry $ entries
  putStrLn "Part 2:"
  print . length . filter validateEntry2 $ entries
